<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;
use App\Helpers\JwtAuth;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth', ['except' => ['index', 'show','getImage']]);
    }

    public function index(){
        $posts = Post::all()->load('category');

        return response() -> json([
            'code'          => 200,
            'status'        => 'success',
            'posts'         => $posts
        ], 200);
    }

    public function show($id){
        $post = Post::find($id)->load('category')
                            ->load('user');

        if(is_object($post)){
            $data = [
                'code'      => 200,
                'status'    => 'success',
                'post'      => $post
            ];
        }else{
            $data = [
                'code'      => 404,
                'status'    => 'error',
                'message'      => 'El post no existe'
            ];
        }

        return response()->json($data, $data['code']);
    }

    public function store(Request $request){
        // Recoger los datos por post
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        if(!empty($params_array)){
            // Conseguir usuario identificado
            $jwtAuth = new JwtAuth();
            $token = $request->header('Authorization', null);
            $user = $jwtAuth->checkToken($token, true);

            // Validar los datos
            $validate = \Validator::make($params_array,[
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required',
                'image' => 'required'
            ]);

            // Guardar la entrada
            if($validate->fails()){
                $data = [
                    'code'      => 404,
                    'status'    => 'error',
                    'message'   => 'No se ha guardado la entrada, faltan datos'
                ];
            }else{
                $post = new Post();
                $post->user_id = $user->sub;
                $post->category_id = $params->category_id;
                $post->title = $params->title;
                $post->content = $params->content;
                $post->image = $params->image;
                $post->save();

                $data = [
                    'code'      => 200,
                    'status'    => 'success',
                    'post'  => $post
                ];
            }

        }else{
            $data = [
                'code'      => 404,
                'status'    => 'error',
                'message'   => 'No has enviado ninguna entrada'
            ];
        }
        // Devolver el resultado
        return response()->json($data, $data['code']);
    }

    public function upload(Request $request){
        // Recoger la imagen de la petición
        $image = $request->file('file0');

        // Validación de imagen
        $validate = \Validator::make($request->all(),[
            'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        // Guardar imagen
        if(!$image || $validate->fails()){

            $data = [
                'code'      =>  400,
                'status'    => 'error',
                'message'   => 'Error al subir imagen.'
            ];

        }else{

            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_name, \File::get($image));

            $data = [
                'code'  => 200,
                'status'=> 'success',
                'image' => $image_name
            ];
        }
        // Devolver datos
        return response()->json($data, $data['code']);
    }

    public function getImage($filename){

        $isset = \Storage::disk('images')->exists($filename);

        if($isset){
            $file = \Storage::disk('images')->get($filename);
            return new Response($file, 200);
        }else{
            $data = array(
                'code'  => 404,
                'status'=> 'error',
                'message' => 'imagen no existe'
            );
            return response()->json($data, $data['code']);
        }
        
    }
}
