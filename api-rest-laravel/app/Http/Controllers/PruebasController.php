<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Category;

class PruebasController extends Controller
{
   public function index(){
       $animales = ["perro", "gato", "tigle"];
       $titulo = "Animales";

       return view('pruebas.index', array('titulo'=>$titulo, 'animales'=>$animales));
   }

   public function testOrm(){
        /*$posts = Post::all();
        foreach($posts as $post){
            echo "<H1>". $post -> title ."</H1>";
            echo "<span> {$post -> user -> name } - {$post -> category -> name} </span>";
            echo "<p>". $post -> content ."</p>";
        }*/

        $categories = Category::all();
        foreach ($categories as $category){
            echo "<h1>". $category->name ."</h1>";

            foreach($category->posts as $post){
                echo "<H3>". $post -> title ."</H3>";
                echo "<span> {$post -> user -> name } - {$post -> category -> name} </span>";
                echo "<p>". $post -> content ."</p>";
            }
        }
        
        die();
   }
}
