<?php

// Cargando clases
use App\Http\Middleware\ApiAuthMiddleware;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/animales', 'PruebasController@index');
Route::get('/test-orm', 'PruebasController@testOrm');

//Route::get('/usuarios-prueba', 'UserController@pruebas');
//Route::get('/post-prueba', 'PostController@pruebas');
//Route::get('/categorias-prueba', 'CategoryController@pruebas');

// RUTAS DELCONTROLADOR DE USUARIOS
Route::post('/api/register', 'UserController@register');
Route::post('/api/login', 'UserController@login');
Route::put('/api/user/update', 'UserController@update');
Route::post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class);
Route::get('/api/user/avatar/{filename}', 'UserController@getImage');
Route::get('/api/user/detail/{id}', 'UserController@detail');

// RUTAS DEL CONTROLADOR DE CATEGORÍAS
Route::resource('/api/category', 'categoryController');

// RUTAS DEL CONTROLADOR DE ENTRADAS
Route::resource('/api/post', 'PostController');
Route::post('/api/post/upload', 'PostController@upload');
Route::get('/api/post/image/{filename}', 'PostController@getImage');