'use strict'

var validator = require('validator');
var bcrypt = require('bcrypt');
var fs = require('fs'); // Para trabajar con archivos
var path = require('path'); // Para trabajar con archivos
var saltRounds = 10;
var Topic = require('../models/topic');
var jwt = require("../services/jwt");

var controller = {

    test: function(req, res){
        return res.status(200).send({
            message: 'Hola que tal'
        });
    },

    save: function(req, res){

        // Recoger los parametros de la petición
        var params = req.body;

        // Validar los datos
        try{
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
            var validate_lang = !validator.isEmpty(params.lang);

        }catch(err){
            return res.status(200).send({
                message: 'Faltan datos por enviar'
            });
        }

       if(validate_title && validate_content && validate_lang){
            // Crear objeto de topic
            var topic = new Topic();

            //Asignar valores al objeto
            topic.title = params.title;
            topic.content = params.content;
            topic.code = params.code;
            topic.lang = params.lang;
            topic.user = req.user.sub;

            // Guardar el topic
            topic.save((err, topicStored) => {

                if(err || !topicStored){
                    return res.status(404).send({
                        message: "El topic no se ha guardado"
                    });
                }

                // Devolver respuesta
                return res.status(200).send({
                    status: 'succes',
                    topic: topicStored});

            }); // close save
            
        }else{
            return res.status(200).send({
                message: 'Validación de los datos incorrectos, inténtalo de nuevo'
            });
        }



    },

    getTopics: function(req, res){

        // Cargar la librería de paginación en la clase (esto se hizo en modelo)

        // Recoger la página actual
        
        if(!req.params.page || req.params.page == null || req.params.page == undefined || req.params.page == 0 || req.params.page == '0'){
            var page = 1;
        }else{
            var page = parseInt(req.params.page); // ParseInt deja un número entero y no como string
        }

        // Indicar las opciones de paginación
        var options = {
            sort: {date: -1}, // -1 del mas nuevo, 1 mas viejo
            populate: 'user',
            limit: 5,
            page: page
        }

        // Find paginado
        Topic.paginate({}, options, (err, topics) =>{

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error al hacer la conuslta'
                });
            }
            if(!topics){
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay topics'
                });
            }

            // Devolver resultado (topics, total de topic, total de páginas)
            return res.status(200).send({
                status: 'success',
                topics: topics.docs,
                totalDocs: topics.totalDocs,
                totalPages: topics.totalPages
            });
        });

    },

    getTopicsByUser: function(req, res){

        // Conseguir el Id del usuario
        var userId = req.params.user;

        // Find con una condición de usuario
        Topic.find({
            user: userId
        })
        .sort([['date', 'descending']])
        .exec((err, topics) =>{

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: "Error en la petición"
                });
            }
            if(!topics){
                return res.status(404).send({
                    status: 'error',
                    message: "No hay temas para mostrar"
                });
            }

            // Devolver un resultado
            return res.status(200).send({
                status: 'success',
                topics
            });
        });

    },

    getTopic: function(req, res){

        var topicId = req.params.id;

        // Find por id del topic
        Topic.findById(topicId)
             .populate('user')
             .populate('comments.user')
             .exec((err, topic) => {

                if(err){
                    return res.status(500).send({
                        status: 'error',
                        message: 'error en la respuesta'
                    });
                }
                if(!topic){
                    return res.status(404).send({
                        status: 'error',
                        message: 'No existe el topic'
                    });
                }

                // Devolver resultado
                return res.status(200).send({
                    status: 'success',
                    topic
                });
             })
    },

    update: function(req, res){

        // Recoger el id del topic de la url
        var topicId = req.params.id;

        // Recoger los datos que llegan desde post
        var params = req.body;

        // Validar datos
        try{
            var validate_title = !validator.isEmpty(params.title);
            var validate_content = !validator.isEmpty(params.content);
            var validate_lang = !validator.isEmpty(params.lang);

        }catch(err){
            return res.status(200).send({
                message: 'Faltan datos por enviar'
            });
        }

        if(validate_title && validate_content && validate_lang){

            // Montar un json con los datos modificables
            var update = {
                title: params.title,
                content: params.content,
                code: params.code,
                lang: params.lang
            };

            // Find and Update del topic por id y id de usuario
            // findOneAndUpdate: busca un doc y actualizalo
            Topic.findOneAndUpdate({_id: topicId, user: req.user.sub}, update, {new:true}, (err, topicUpdated) => {
                
                if(err){
                    return res.status(500).send({
                        status: 'error',
                        message: 'error en la petición'
                    });
                }
                if(!topicUpdated){
                    return res.status(404).send({
                        status: 'error',
                        message: 'El topic no se ha actualizado'
                    });
                }

                // Devolver resultado
                return res.status(200).send({
                    status: 'success',
                    topic: topicUpdated
                });
            });

        }else{
            return res.status(200).send({
                message: 'La validación de los datos no es correcta'
            });
        }

        
    },

    delete: function(req, res){

        // Sacar id de topic de la url
        var topicId = req.params.id;

        // Find and delete
        Topic.findByIdAndDelete({_id: topicId, user: req.user.sub}, (err, topicRemoved) => {
            
            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'error en la petición'
                });
            }
            if(!topicRemoved){
                return res.status(404).send({
                    status: 'error',
                    message: 'El topic no se ha eliminado'
                });
            }

            return res.status(200).send({
                status: 'success',
                topic: topicRemoved
            });
        });

    },

    search: function(req, res){

        // Sacar string a buscar
        var searchString = req.params.search;

        // Find or
        Topic.find({'$or': [
            { 'title': {'$regex': searchString, '$options': '1'}},
            { 'content': {'$regex': searchString, '$options': '1'}},
            { 'code': {'$regex': searchString, '$options': '1'}},
            { 'lang': {'$regex': searchString, '$options': '1'}}         
        ]})
        .populate('user')
        .sort([['date', 'descending']])
        .exec((err, topics) => {

            if(err){
                return res.status(500).send({
                    status: 'error',
                    message: 'Error en la petición'
                });
            }
            if(!topics){
                return res.status(404).send({
                    status: 'error',
                    message: 'No hay temas disponibles'
                });
            }
            
            return res.status(200).send({
                status: 'success',
                topics
            });
            

        });
    }

};
module.exports = controller;