'use strict'

var mongoose = require("mongoose");
var app = require('./app');
var port = process.env.PORT || 3999;

mongoose.set('useFindAndModify', false);
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/api_rest_node", {useNewUrlParser:true})
        .then(() =>{
            console.log("la conección a la BD de Mongo se ha realizado");

            // Crear el servidor
            app.listen(port, () =>{
                console.log('el servidor http://localhost:3999 esta corriendo');
            });
        })
        .catch(error => console.log(error));